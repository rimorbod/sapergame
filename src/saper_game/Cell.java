package saper_game;

public class Cell {
    private CellState content;
    private CellState currentContent;
    private boolean cover;
    private int row;
    private int col;

    public Cell(int row, int col){
        this.row=row;
        this.col=col;
        cover();
    }

    public void cover(){
        cover = true;
        content = CellState.EMPTY;
        currentContent = CellState.EMPTY;
    }

    public void paint(){
        switch (currentContent) {
            case BOMB:
                System.out.print("JEB");
                break;
            case FLAG:
                System.out.print(" X ");
                break;
            case EMPTY:
                System.out.print(" O ");
                break;
        }
    }

    public CellState getContent() {
        return content;
    }


    public void setContent(CellState content) {
        this.content = content;
    }


    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public boolean getCover(){
        return cover;
    }

    public void setCover(boolean cover) {
        this.cover = cover;
    }

    public CellState getCurrentContent() {
        return currentContent;
    }

    public void setCurrentContent(CellState currentContent) {
        this.currentContent = currentContent;
    }
}
