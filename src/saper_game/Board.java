package saper_game;

import java.util.Random;

public class Board {
    private static final int ROWS = 8;
    private static final int COLS = 8;
    private static final int BOMBS = 10;

    private Cell[][] cells;
    private int currentRow;
    private int currentCol;


    private void randBombs(Cell[][] cells, int bombs) {
         for (int i=0; i<bombs; i++){
             randBomb(cells);
         }
    }

    private void randBomb(Cell[][] cells) {
        Random r = new Random();
        int randomRow = r.nextInt(8);
        int randomCol = r.nextInt(8);
        if(cells[randomRow][randomCol].getContent().equals(CellState.EMPTY)) {
            cells[randomRow][randomCol].setContent(CellState.BOMB);
        }else{
            randBomb(cells);
        }
    }


    public void paint(){
         if (bombLeft()==10||bombLeft()<0){
             System.out.println(bombLeft()+"| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |");
         }else {
             System.out.println(bombLeft() + "F| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |");
         }
        System.out.println("===================================");
        for (int row=0; row<ROWS; row++){
            System.out.print(row+1+"||");
            for (int col=0; col<COLS; col++) {
                if (!cells[row][col].getCover() && cells[row][col].getCurrentContent().equals(CellState.EMPTY)){
                    checkBombs(cells, row, col);
                    printBombs(checkBombs(cells, row, col));
                }else {
                    cells[row][col].paint();
                }
                System.out.print("|");
            }
            System.out.println();
            if (row<ROWS){
                System.out.println("-----------------------------------");
            }
        }
    }

    private int bombLeft() {
        int flags = BOMBS;
        for(int row=0; row<ROWS; row++){
            for(int col=0; col<COLS; col++){
                if (cells[row][col].getCurrentContent().equals(CellState.FLAG)){
                    flags--;
                }
            }
        }
        return flags;
    }

    private void printBombs(int bombs) {
        switch (bombs){
            case 0:
                System.out.print("   ");
                break;
            case 1:
                System.out.print(" 1 ");
                break;
            case 2:
                System.out.print(" 2 ");
                break;
            case 3:
                System.out.print(" 3 ");
                break;
            case 4:
                System.out.print(" 4 ");
                break;
            case 5:
                System.out.print(" 5 ");
                break;
            case 6:
                System.out.print(" 6 ");
                break;
            case 7:
                System.out.print(" 7 ");
                break;
            case 8:
                System.out.print(" 8 ");
                break;
        }
    }

    private int countColumns(Cell[][] cells, int col, int bombs, int i) {
        if(col>0 && col<COLS-1) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (cells[i][j].getContent().equals(CellState.BOMB)) {
                    bombs++;
                }
            }
        }else if(col<COLS-1){
            for (int j = col; j <= col + 1; j++) {
                if (cells[i][j].getContent().equals(CellState.BOMB)) {
                    bombs++;
                }
            }
        }else{
            for (int j = col - 1; j < col + 1; j++) {
                if (cells[i][j].getContent().equals(CellState.BOMB)) {
                    bombs++;
                }
            }
        }
        return bombs;
    }

    public Board(){
        cells = new Cell[ROWS][COLS];
        for(int row=0; row<ROWS; row++){
            for (int col=0; col<COLS; col++){
                cells[row][col] = new Cell(row,col);
            }
        }
    }
    public void init(){
        for (int row=0; row<ROWS; row++){
            for (int col=0; col<COLS; col++){
                cells[row][col].cover();
            }
        }
        randBombs(cells, BOMBS);
    }

    public int checkBombs(Cell[][] cells, int row, int col) {
        int bombs = 0;
        if (row>0 && row<ROWS-1) {
            for (int i = row - 1; i <= row + 1; i++) {
                bombs = countColumns(cells, col, bombs, i);
            }
        }else if(row<ROWS-1){
            for (int i=row; i<=row+1; i++){
                bombs = countColumns(cells,col,bombs,i);
            }
        }else{
            for (int i=row-1; i<row+1; i++){
                bombs = countColumns(cells,col,bombs,i);
            }
        }
        return bombs;
    }


    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public static int getCOLS() {
        return COLS;
    }

    public static int getROWS() {
        return ROWS;
    }

    public static int getBOMBS() {
        return BOMBS;
    }

    public int getCurrentCol() {
        return currentCol;
    }

    public void setCurrentCol(int currentCol) {
        this.currentCol = currentCol;
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }

    public boolean hasLost() {
        for (int row = 0; row<ROWS; row++){
            for(int col = 0; col<COLS; col++){
                if (cells[row][col].getCurrentContent().equals(CellState.BOMB)){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasWon() {
        int covered = 0;
        for (int row = 0; row<ROWS; row++){
            for(int col = 0; col<COLS; col++){
                if (cells[row][col].getCover()){
                    covered++;
                    if (covered>10){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
