package saper_game;

import java.util.Scanner;

public class SaperManager {
    private Board board;
    private GameState currentState;

    private static Scanner scanner = new Scanner(System.in);

    private void initGame(){
        board.init();
        currentState = GameState.PLAYING;
    }

    public void start(){
        board = new Board();
        initGame();
        do{
            move();
            board.paint();
            updateGame();
            if (currentState.equals(GameState.WIN)){
                System.out.println("Good job, You won!");
            } else if (currentState.equals(GameState.LOOSE)){
                System.out.println("You loose, try again!");
            }
        }while (currentState.equals(GameState.PLAYING));
    }

    private void updateGame() {
        if (board.hasLost()) {
            currentState=GameState.LOOSE;
        } else if (board.hasWon()){
            currentState=GameState.WIN;
        }
    }

    private void move() {
        boolean validInput = false;
        do{
            System.out.println("Enter position (row [1-8] column [1-8]):");
            int row = scanner.nextInt()-1;
            int col = scanner.nextInt()-1;
            Cell[][] boardCells = board.getCells();
            if (row>=0 && row<Board.getROWS() && col>=0 && col<Board.getCOLS() && boardCells[row][col].getCover()){
                board.setCurrentRow(row);
                board.setCurrentCol(col);
                do{
                    System.out.println("Choose \"U\" to uncover, or \"F\" to point a flag");
                    String decision = scanner.next().toUpperCase();
                    if (decision.equals("F")){
                        boardCells[row][col].setCurrentContent(CellState.FLAG);
                        validInput=true;
                    }else if (decision.equals("U")){
                        uncover(row, col, boardCells);
                        validInput=true;
                    }else{
                        System.out.println("Invalid operation: "+decision+" , try again");
                    }
                }while (!validInput);

            } else{
                System.out.println("Invalid position (row: "+row+" col: "+col+"), try again");
            }
        }while (!validInput);
    }

    private void uncover(int row, int col, Cell[][] boardCells) {
        boardCells[row][col].setCurrentContent(boardCells[row][col].getContent());
        boardCells[row][col].setCover(false);
        if (board.checkBombs(boardCells, row, col)==0){
            if (row>0 && row<7){
                for (int i=row-1; i<=row+1; i++){
                    uncoverColumns(col, boardCells, i);
                }
            } else if(row>0){
                for (int i=row-1; i<=row; i++){
                    uncoverColumns(col, boardCells, i);
                }
            } else {
                for (int i=row; i<=row+1; i++){
                    uncoverColumns(col, boardCells, i);
                }
            }
        }
    }

    private void uncoverColumns(int col, Cell[][] boardCells, int i) {
        if (col > 0 && col < 7) {
            for (int j=col-1; j<=col+1; j++){
                if (boardCells[i][j].getCover()) {
                    uncover(i, j, boardCells);
                }
            }
        } else if(col>0){
            for (int j=col-1; j<=col; j++){
                if (boardCells[i][j].getCover()) {
                    uncover(i, j, boardCells);
                }
            }
        } else{
            for (int j=col; j<=col+1; j++){
                if (boardCells[i][j].getCover()) {
                    uncover(i, j, boardCells);
                }
            }
        }
    }

}
