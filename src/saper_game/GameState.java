package saper_game;

public enum GameState {
    PLAYING, WIN, LOOSE;
}
